#!/bin/bash

# Setup confd templates
mv /build/gsearch/confd/conf.d/* /etc/confd/conf.d/
mv /build/gsearch/confd/templates/* /etc/confd/templates/

# Install GSearch
unzip /build/downloads/fedoragsearch.war -d $CATALINA_HOME/webapps/fedoragsearch
rm /build/downloads/fedoragsearch.war

rm /usr/local/tomcat/webapps/fedoragsearch/WEB-INF/classes/log4j.xml
mv /build/gsearch/log4j.properties $CATALINA_HOME/webapps/fedoragsearch/WEB-INF/classes/log4j.properties

rm -Rf /usr/local/tomcat/webapps/fedoragsearch/WEB-INF/classes/fgsconfigFinal
mv /build/gsearch/fgsconfigFinal /usr/local/tomcat/webapps/fedoragsearch/WEB-INF/classes
mkdir -p /usr/local/tomcat/webapps/fedoragsearch/WEB-INF/classes/fgsconfigFinal/updater/FgsUpdaters
mkdir -p /opt/fedora/data/gsearch/FgsIndex
