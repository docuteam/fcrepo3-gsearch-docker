#!/bin/bash

# Install some required packages
apk add --no-cache unzip
apk add --no-cache ca-certificates
apk add --no-cache openssl
update-ca-certificates

# Install confd for templating our configuration
cp /build/fedora/confd/templates/* /etc/confd/templates/
cp /build/fedora/confd/conf.d/* /etc/confd/conf.d/

# Install Fedora Commons 3
java -jar /build/downloads/fcrepo-installer.jar /build/fedora/install.properties
rm -Rf /opt/fedora/install
rm -Rf /opt/fedora/docs
rm /build/downloads/fcrepo-installer.jar

unzip $CATALINA_HOME/webapps/fedora.war -d $CATALINA_HOME/webapps/fedora
rm $CATALINA_HOME/webapps/fedora.war
rm $CATALINA_HOME/webapps/fedora/WEB-INF/lib/jsr311-api-1.0.jar
mkdir /opt/fedora/data
mv /build/fedora/logback.xml /opt/fedora/server/config/logback.xml
mv /build/fedora/akubra-llstore.xml /opt/fedora/server/config/spring/akubra-llstore.xml
mv /build/fedora/xacml /opt/fedora/data/fedora-xacml-policies
