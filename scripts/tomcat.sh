#!/bin/bash

# Setup Tomcat8.5
rm -Rf $CATALINA_HOME/webapps/*

cp /build/tomcat/log4j.properties $CATALINA_HOME/lib/log4j.properties
cp /build/tomcat/server.xml $CATALINA_HOME/conf/server.xml
