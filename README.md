# Docker container with Fedora3 and GSearch

Docker image containing Fedora Commons Repository 3.8.1 and GSearch with docuteam default configuration for use in integration testing.

The built docker image is available on [hub.docker.com/docuteam/fedora3-gsearch](https://hub.docker.com/r/docuteam/fedora3-gsearch)

This repository used https://github.com/jonathangreen/fcrepo3-gsearch-docker as a starting point, addapted the configuration for docuteam specific needs and adding a pipeline to push the image to the docker hub. All Islandora-specific configuration and and fedoragsearch-extensions have been removed.
