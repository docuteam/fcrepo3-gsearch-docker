<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
				xmlns:foxml="info:fedora/fedora-system:def/foxml#"
				xmlns:audit="info:fedora/fedora-system:def/audit#"
				xmlns:dc="http://purl.org/dc/elements/1.1/"
				xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
				xmlns:dtu_meta="http://www.dtu.dk/dtu_meta/"
				xmlns:meta="http://www.dtu.dk/dtu_meta/meta/"
				xmlns:exts="xalan://dk.defxws.fedoragsearch.server.GenericOperationsImpl"
				xmlns:EAD="urn:isbn:1-931666-22-9"
				xmlns:xlink="http://www.w3.org/1999/xlink"
				xmlns:PREMIS="info:lc/xmlns/premis-v2"
				xmlns:fedora-model="info:fedora/fedora-system:def/model#"
				xmlns:fedora="info:fedora/fedora-system:def/relations-external#"
				xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
				version="1.0"
				exclude-result-prefixes="exts">
	<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
	<xsl:param name="REPOSITORYNAME" select="'FgsRepos'"/>
	<xsl:param name="REPOSBASEURL" select="'http://localhost:8080/fedora'"/>
	<xsl:param name="FEDORASOAP" select="'http://localhost:8080/fedora/services'"/>
	<xsl:param name="FEDORAUSER" select="'fedoraAdmin'"/>
	<xsl:param name="FEDORAPASS" select="'fedoraAdmin'"/>
    <xsl:param name="TRUSTSTOREPATH" select="''"/>
	<xsl:param name="TRUSTSTOREPASS" select="''"/>
	<xsl:variable name="PID" select="/foxml:digitalObject/@PID"/>
	<xsl:template match="/">
		<IndexDocument boost="1.0">
			<xsl:attribute name="PID">
				<xsl:value-of select="$PID"/>
			</xsl:attribute>
			<!--The PID attribute is mandatory for indexing to work-->
			<!--The following allows only active FedoraObjects to be indexed.-->
			<xsl:if test="foxml:digitalObject/foxml:objectProperties/foxml:property[@NAME='info:fedora/fedora-system:def/model#state' and @VALUE='Active']">
				<xsl:if test="not(foxml:digitalObject/foxml:datastream[@ID='METHODMAP'] or foxml:digitalObject/foxml:datastream[@ID='DS-COMPOSITE-MODEL'])">
					<xsl:if test="starts-with($PID,'')">
						<xsl:apply-templates mode="activeFedoraObject"/>
					</xsl:if>
				</xsl:if>
			</xsl:if>
		</IndexDocument>
	</xsl:template>
	<xsl:template match="/foxml:digitalObject" mode="activeFedoraObject"><!--The PID index field lets you search on the PID value-->
		<IndexField IFname="PID"
					index="UN_TOKENIZED"
					store="YES"
					termVector="NO"
					boost="1.0"
					displayName="PID">
			<xsl:value-of select="$PID"/>
		</IndexField>
		<IndexField IFname="PIDNS"
					index="UN_TOKENIZED"
					store="YES"
					termVector="NO"
					boost="1.0"
					displayName="PIDNS">
			<xsl:value-of select="substring-before($PID, ':')"/>
		</IndexField>
		<IndexField IFname="REPOSITORYNAME"
					index="UN_TOKENIZED"
					store="YES"
					termVector="NO"
					boost="1.0">
			<xsl:value-of select="$REPOSITORYNAME"/>
		</IndexField>
		<IndexField IFname="REPOSBASEURL"
					index="UN_TOKENIZED"
					store="YES"
					termVector="NO"
					boost="1.0">
			<xsl:value-of select="substring($FEDORASOAP, 1, string-length($FEDORASOAP)-9)"/>
		</IndexField>
		<IndexField IFname="TITLE_UNTOK"
					index="UN_TOKENIZED"
					store="YES"
					termVector="NO"
					boost="1.0">
			<xsl:value-of select="foxml:datastream/foxml:datastreamVersion[last()]/foxml:xmlContent/oai_dc:dc/dc:title"/>
		</IndexField>
		<IndexField IFname="AUTHOR_UNTOK"
					index="UN_TOKENIZED"
					store="YES"
					termVector="NO"
					boost="1.0">
			<xsl:value-of select="foxml:datastream/foxml:datastreamVersion[last()]/foxml:xmlContent/oai_dc:dc/dc:creator"/>
		</IndexField>
		<!-- foxml properties start -->
		<xsl:for-each select="foxml:objectProperties/foxml:property">
			<IndexField index="UN_TOKENIZED" store="YES" termVector="NO">
				<xsl:attribute name="IFname">
					<xsl:value-of select="concat('fgs.', substring-after(@NAME,'#'))"/>
				</xsl:attribute>
				<xsl:value-of select="@VALUE"/>
			</IndexField>
		</xsl:for-each>
		<!-- foxml properties end -->
		<!-- EAD start -->
		<IndexField IFname="EAD.unittitle"
					index="UN_TOKENIZED"
					store="YES"
					termVector="YES"
					boost="1.0"
					displayName="EAD.unittitle">
			<xsl:value-of select="//EAD:c/EAD:did/EAD:unittitle[@label='main']"/>
		  </IndexField>
		<IndexField	IFname="EAD.level"
					index="UN_TOKENIZED"
					store="YES"
					termVector="NO"
					boost="1.0"
					displayName="EAD.level">
			<xsl:value-of select="//EAD:c/@otherlevel"/>
		</IndexField>
		<xsl:for-each select="//EAD:did/EAD:abstract">
			<IndexField IFname="EAD.abstract"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.abstract">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:acqinfo/EAD:p">
			<IndexField IFname="EAD.accessNr"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.accessNr">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:accessrestrict[@type = 'accessPolicy']/EAD:p">
			<IndexField IFname="EAD.accessPolicy"
					index="UN_TOKENIZED"
						  store="YES"
						  termVector="YES"
						  boost="1.0"
						  displayName="EAD.accessPolicy">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:accessrestrict[@type = 'restrictions']/EAD:p">
			<IndexField IFname="EAD.accessRestriction"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.accessRestriction">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:accessrestrict[@type = 'accessRestrictionsClassification']/EAD:p">
			<IndexField IFname="EAD.accessRestrictionClassification"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.accessRestrictionClassification">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:accessrestrict[@type = 'accessRestrictionsExplanation']/EAD:p">
			<IndexField IFname="EAD.accessRestrictionExplanation"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.accessRestrictionExplanation">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:accessrestrict[@type = 'accessRestrictionsPeriod']/EAD:p">
			<IndexField IFname="EAD.accessRestrictionPeriod"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.accessRestrictionPeriod">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:accessrestrict[@type = 'accessRestrictionsPeriodBaseYear']/EAD:p">
			<IndexField IFname="EAD.accessRestrictionPeriodBaseYear"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.accessRestrictionPeriodBaseYear">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:accessrestrict[@type = 'accessRestrictionsPrivacy']/EAD:p">
			<IndexField IFname="EAD.accessRestrictionPrivacy"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.accessRestrictionPrivacy">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:accessrestrict[@type = 'accessRestrictionsStatus']/EAD:p">
			<IndexField IFname="EAD.accessRestrictionStatus"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.accessRestrictionStatus">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:accessrestrict[@type = 'accessRestrictionsStatusExplanation']/EAD:p">
			<IndexField IFname="EAD.accessRestrictionStatusExplanation"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.accessRestrictionStatusExplanation">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:appraisal/EAD:p">
			<IndexField IFname="EAD.appraisalAndDestruction"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.appraisalAndDestruction">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:custodhist/EAD:p">
			<IndexField IFname="EAD.archivalHistory"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.archivalHistory">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:arrangement/EAD:p">
			<IndexField IFname="EAD.arrangement"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.arrangement">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:controlaccess/EAD:persname[@role = 'author']">
			<IndexField IFname="EAD.author"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.author">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:bibliography/EAD:p">
			<IndexField IFname="EAD.bibliography"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.bibliography">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:phystech/EAD:p">
			<IndexField IFname="EAD.characteristics"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.characteristics">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:note/EAD:p">
			<IndexField IFname="EAD.comment"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.comment">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:controlaccess/EAD:name[@role = 'compartment']">
			<IndexField IFname="EAD.compartment"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.compartment">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:userestrict/EAD:p">
			<IndexField IFname="EAD.conditionsOfReproductions"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.conditionsOfReproductions">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:did/EAD:unitdate[@label = 'creationPeriod']">
			<IndexField IFname="EAD.creationPeriod"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.creationPeriod">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:did/EAD:unitdate[@label = 'creationPeriodNotes']">
			<IndexField IFname="EAD.creationPeriodNotes"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.creationPeriodNotes">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:processinfo[@type = 'level']/EAD:p">
			<IndexField IFname="EAD.descriptionLevel"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.descriptionLevel">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:processinfo[@type = 'levelNotes']/EAD:p">
			<IndexField IFname="EAD.descriptionLevelNotes"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.descriptionLevelNotes">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:processinfo[@type = 'rules']/EAD:p">
			<IndexField IFname="EAD.descriptionRules"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.descriptionRules">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:odd[@type = 'doiJournal']/EAD:p">
			<IndexField IFname="EAD.doiJournal"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.doiJournal">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:did/EAD:physdesc/EAD:extent">
			<IndexField IFname="EAD.extent"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.extent">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:otherfindaid/EAD:p">
			<IndexField IFname="EAD.findingAids"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.findingAids">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:did/EAD:unitdate[@label = 'fromYear']">
			<IndexField IFname="EAD.fromYear"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.fromYear">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:controlaccess/EAD:name[@role = 'fundingSource']">
			<IndexField IFname="EAD.fundingSource"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.fundingSource">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:controlaccess/EAD:name[@role = 'institute']">
			<IndexField IFname="EAD.institute"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.institute">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:controlaccess/EAD:corpname">
			<IndexField IFname="EAD.institution"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.institution">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:controlaccess/EAD:persname[@role = 'involved']">
			<IndexField IFname="EAD.involved"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.involved">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:controlaccess/EAD:name[@role = 'journal']">
			<IndexField IFname="EAD.journal"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.journal">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:controlaccess/EAD:name[@role = 'keyword']">
			<IndexField IFname="EAD.keyword"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.keyword">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:did/EAD:langmaterial/EAD:language">
			<IndexField IFname="EAD.language"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.language">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:did/EAD:langmaterial">
			<IndexField IFname="EAD.languageNotes"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.languageNotes">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:controlaccess/EAD:name[@role = 'location']">
			<IndexField IFname="EAD.location"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.location">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:originalsloc/EAD:p">
			<IndexField IFname="EAD.locationOfOriginals"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.locationOfOriginals">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:did/EAD:physdesc/EAD:physfacet">
			<IndexField IFname="EAD.material"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.material">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:controlaccess/EAD:name[@role = 'method']">
			<IndexField IFname="EAD.method"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.method">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:controlaccess/EAD:genreform">
			<IndexField IFname="EAD.objectType"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.objectType">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:did/EAD:origination">
			<IndexField IFname="EAD.origination"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.origination">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:dao[@xlink:role = 'simple']">
			<IndexField IFname="EAD.PID"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.PID">
				<xsl:value-of select="@xlink:href"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:odd[@type = 'project']/EAD:p">
			<IndexField IFname="EAD.project"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.project">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:odd[@type = 'projectAbbreviation']/EAD:p">
			<IndexField IFname="EAD.projectAbbreviation"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.projectAbbreviation">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:odd[@type = 'projectName']/EAD:p">
			<IndexField IFname="EAD.projectName"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.projectName">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:odd[@type = 'projectTitle']/EAD:p">
			<IndexField IFname="EAD.projectTitle"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.projectTitle">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:controlaccess/EAD:name[@role = 'publisher']">
			<IndexField IFname="EAD.publisher"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.publisher">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:did/EAD:unitid[@type = 'refCode']">
			<IndexField IFname="EAD.refCode"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.refCode">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:did/EAD:unitid[@type = 'refCodeAdmin']">
			<IndexField IFname="EAD.refCodeAdmin"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.refCodeAdmin">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:did/EAD:unitid[@type = 'refCodeOld']">
			<IndexField IFname="EAD.refCodeOld"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.refCodeOld">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:relatedmaterial/EAD:p">
			<IndexField IFname="EAD.relatedMaterial"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.relatedMaterial">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:altformavail/EAD:p">
			<IndexField IFname="EAD.reproductions"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.reproductions">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:controlaccess/EAD:persname[@role = 'responsible']">
			<IndexField IFname="EAD.responsible"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.responsible">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:accessrestrict[@type = 'retentionPeriod']/EAD:p">
			<IndexField IFname="EAD.retentionPeriod"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.retentionPeriod">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:accessrestrict[@type = 'baseYear']/EAD:p">
			<IndexField IFname="EAD.retentionPeriodBaseYear"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.retentionPeriodBaseYear">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:accessrestrict[@type = 'retentionPolicy']/EAD:p">
			<IndexField IFname="EAD.retentionPolicy"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.retentionPolicy">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:processinfo[@type = 'revisions']/EAD:p">
			<IndexField IFname="EAD.revisions"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.revisions">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:scopecontent/EAD:p">
			<IndexField IFname="EAD.scopeContent"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.scopeContent">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:controlaccess/EAD:persname[@role = 'staff']">
			<IndexField IFname="EAD.staff"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.staff">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:controlaccess/EAD:name[@role = 'status']">
			<IndexField IFname="EAD.status"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.status">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:did/EAD:unitdate[@label = 'toYear']">
			<IndexField IFname="EAD.toYear"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.toYear">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:did/EAD:unittitle[@label = 'additional']">
			<IndexField IFname="EAD.unitTitleAdditional"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.unitTitleAdditional">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:controlaccess/EAD:name[@role = 'university']">
			<IndexField IFname="EAD.university"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.university">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:odd[@type = 'usage']/EAD:p">
			<IndexField IFname="EAD.usage"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.usage">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//EAD:did/EAD:unitdate[@label = 'year']">
			<IndexField IFname="EAD.year"
						index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						displayName="EAD.year">
				<xsl:value-of select="text()"/>
			</IndexField>
		</xsl:for-each>
		<!-- EAD end -->
		<!-- PREMIS object start -->
		<IndexField index="UN_TOKENIZED"
			store="YES"
			termVector="YES"
			boost="1.0"
			IFname="PREMIS.formatName"
			displayName="PREMIS.formatName">
			<xsl:value-of select="(//PREMIS:formatName)[last()]"/>
		</IndexField>
		<IndexField index="UN_TOKENIZED"
			store="YES"
			termVector="YES"
			boost="1.0"
			IFname="PREMIS.formatRegistryKey"
			displayName="PREMIS.formatRegistryKey">
			<xsl:value-of select="(//PREMIS:formatRegistryKey)[last()]"/>
		</IndexField>
		<IndexField index="UN_TOKENIZED"
			store="YES"
			termVector="YES"
			boost="1.0"
			IFname="PREMIS.formatRegistryName"
			displayName="PREMIS.formatRegistryName">
			<xsl:value-of select="(//PREMIS:formatRegistryName)[last()]"/>
		</IndexField>
		<IndexField index="UN_TOKENIZED"
			store="YES"
			termVector="YES"
			boost="1.0"
			IFname="PREMIS.messageDigest"
			displayName="PREMIS.messageDigest">
			<xsl:value-of select="(//PREMIS:messageDigest)[last()]"/>
		</IndexField>
		<IndexField index="UN_TOKENIZED"
			store="YES"
			termVector="YES"
			boost="1.0"
			IFname="PREMIS.messageDigestAlgorithm"
			displayName="PREMIS.messageDigestAlgorithm">
			<xsl:value-of select="(//PREMIS:messageDigestAlgorithm)[last()]"/>
		</IndexField>
		<IndexField index="UN_TOKENIZED"
			store="YES"
			termVector="NO"
			boost="1.0"
			IFname="PREMIS.object_xsi:type"
			displayName="PREMIS.object_xsi:type">
			<xsl:value-of select="(//PREMIS:object)[last()]/@xsi:type"/>
		</IndexField>
		<IndexField index="UN_TOKENIZED"
			store="YES"
			termVector="YES"
			boost="1.0"
			IFname="PREMIS.originalName"
			displayName="PREMIS.originalName">
			<xsl:value-of select="(//PREMIS:originalName)[last()]"/>
		</IndexField>
		<IndexField index="UN_TOKENIZED"
			store="YES"
			termVector="YES"
			boost="1.0"
			IFname="PREMIS.size"
			displayName="PREMIS.size">
			<xsl:value-of select="(//PREMIS:size)[last()]"/>
		</IndexField>
		<!-- PREMIS object end -->
		<!-- PREMIS event start -->
		<IndexField index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						IFname="PREMIS.eventDateTime"
						displayName="PREMIS.eventDateTime">
			<xsl:value-of select="(//PREMIS:eventDateTime)[last()]"/>
		</IndexField>
		<IndexField index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						IFname="PREMIS.eventDetail"
						displayName="PREMIS.eventDetail">
			<xsl:value-of select="(//PREMIS:eventDetail)[last()]"/>
		</IndexField>
		<IndexField index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						IFname="PREMIS.eventOutcome"
						displayName="PREMIS.eventOutcome">
			<xsl:value-of select="(//PREMIS:eventOutcome)[last()]"/>
		</IndexField>
		<IndexField index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						IFname="PREMIS.eventType"
						displayName="PREMIS.eventType">
			<xsl:value-of select="(//PREMIS:eventType)[last()]"/>
		</IndexField>
		<!-- PREMIS event end -->
		<!-- DC  -->
		<IndexField index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						IFname="dc.identifier"
						displayName="dc.identifier">
			<xsl:value-of select="(//dc:identifier)[last()]"/>
		</IndexField>
		<IndexField index="UN_TOKENIZED"
						store="YES"
						termVector="YES"
						boost="1.0"
						IFname="dc.title"
						displayName="dc.title">
			<xsl:value-of select="(//dc:title)[last()]"/>
		</IndexField>
		<xsl:for-each select="//fedora-model:hasModel">
			<IndexField index="UN_TOKENIZED"
							store="YES"
							termVector="NO"
							boost="1.0"
							IFname="fedora-model.hasModel"
							displayName="fedora-model.hasModel">
				<xsl:value-of select="@rdf:resource"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//fedora:hasPart">
			<IndexField index="UN_TOKENIZED"
							store="YES"
							termVector="NO"
							boost="1.0"
							IFname="fedora.hasPart"
							displayName="fedora.hasPart">
				<xsl:value-of select="@rdf:resource"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//fedora:isMemberOf">
			<IndexField index="UN_TOKENIZED"
							store="YES"
							termVector="NO"
							boost="1.0"
							IFname="fedora.isMemberOf"
							displayName="fedora.isMemberOf">
				<xsl:value-of select="@rdf:resource"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//fedora:isPartOf">
			<IndexField index="UN_TOKENIZED"
							store="YES"
							termVector="NO"
							boost="1.0"
							IFname="fedora.isPartOf"
							displayName="fedora.isPartOf">
				<xsl:value-of select="@rdf:resource"/>
			</IndexField>
		</xsl:for-each>
		<IndexField index="UN_TOKENIZED"
						store="YES"
						termVector="NO"
						boost="1.0"
						IFname="foxml.datastreamVersion_MIMETYPE"
						displayName="foxml.datastreamVersion_MIMETYPE">
			<xsl:value-of select="//foxml:datastream[@ID='ORIGINAL']/foxml:datastreamVersion[last()]/@MIMETYPE"/>
		</IndexField>
		<IndexField index="UN_TOKENIZED"
						store="YES"
						termVector="NO"
						boost="1.0"
						IFname="foxml.datastreamVersion_SIZE"
						displayName="foxml.datastreamVersion_SIZE">
			<xsl:value-of select="//foxml:datastream[@ID='ORIGINAL']/foxml:datastreamVersion[last()]/@SIZE"/>
		</IndexField>
		<xsl:for-each select="//foxml:digitalObject">
			<IndexField index="UN_TOKENIZED"
							store="YES"
							termVector="NO"
							boost="1.0"
							IFname="foxml.digitalObject_PID"
							displayName="foxml.digitalObject_PID">
				<xsl:value-of select="@PID"/>
			</IndexField>
		</xsl:for-each>
		<xsl:for-each select="//foxml:digitalObject">
			<IndexField index="UN_TOKENIZED"
							store="YES"
							termVector="NO"
							boost="1.0"
							IFname="foxml.digitalObject_VERSION"
							displayName="foxml.digitalObject_VERSION">
				<xsl:value-of select="@VERSION"/>
			</IndexField>
		</xsl:for-each>
		<!-- a datastream is fetched, if its mimetype
				  can be handled, the text becomes the value of the field.
				  This is the version using PDFBox,
				  below is the new version using Apache Tika. -->
		<!---->
		<!-- Text and metadata extraction using Apache Tika. -->
		<xsl:for-each select="//foxml:datastream[@ID='ORIGINAL']">
			<xsl:choose>
				<xsl:when test="foxml:datastreamVersion[
					@MIMETYPE='application/pdf'
					or @MIMETYPE='text/plain'
					or @MIMETYPE='text/xml'
					or @MIMETYPE='text/richtext'
					or @MIMETYPE='text/rtf'
					or @MIMETYPE='text/html'
					or @MIMETYPE='text/tab-separated-values']">
					<xsl:value-of disable-output-escaping="yes"
		 select="exts:getDatastreamFromTika($PID, $REPOSITORYNAME, @ID, 'IndexField', concat('ds.', @ID), concat('dsmd.', @ID, '.'), '', $FEDORASOAP, $FEDORAUSER, $FEDORAPASS, $TRUSTSTOREPATH, $TRUSTSTOREPASS)"/>
				</xsl:when>
				<xsl:otherwise>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
		<!--creating an index field with all text from the foxml record and its datastreams-->
		<!-- disabled, Nf/Docuteam, 04.12.2014
		<IndexField IFname="foxml.all.text"
						index="TOKENIZED"
						store="YES"
						termVector="YES">
			<xsl:for-each select="//text()">
				<xsl:value-of select="."/>
				<xsl:text> </xsl:text>
			</xsl:for-each>
			<xsl:for-each select="//foxml:datastream[@CONTROL_GROUP='M' or @CONTROL_GROUP='E' or @CONTROL_GROUP='R']">
				<xsl:value-of select="exts:getDatastreamText($PID, $REPOSITORYNAME, @ID, $FEDORASOAP, $FEDORAUSER, $FEDORAPASS, $TRUSTSTOREPATH, $TRUSTSTOREPASS)"/>
				<xsl:text> </xsl:text>
			</xsl:for-each>
		</IndexField> -->
	</xsl:template>
</xsl:stylesheet>
