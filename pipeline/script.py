#!/usr/bin/python3

import sys
import getopt

import unittest
import requests

######################
### reading params ###
######################

usage = """ 
Usage
-----
python3 script.py --baseurl=<baseurl> --fedoraUser=<user> --fedoraPasword=<password>

Example
-------
python3 script.py --baseurl="http://fedora-gsearch:8080/" --fedoraUser=fedoraAdmin --fedoraPassword=fedoraAdmin

"""
params_list = ["baseurl=", "fedoraUser=", "fedoraPassword="]

try:
    opts, args = getopt.getopt(sys.argv[1:], "", params_list)
except Exception as e:
    print(e)
    print(usage)
    sys.exit(2)

params = {}
for x in opts:
    params[x[0]] = x[1]
    
for p in params_list:
    if not '--'+p.replace('=','') in params.keys():
        print('{} is mandatory'.format(p))
        print(usage)
        sys.exit(2)

#############
### tests ###
#############

class TestFedoraGsearch(unittest.TestCase):

    baseurl =  params['--baseurl'] #'http://localhost:8080/'
    username = params['--fedoraUser'] #'fedoraAdmin'
    password = params['--fedoraPassword'] #'fedoraAdmin'

    def test_001_fedora_service_up(self):
        url = self.baseurl+'fedora/describe'
        print('URL:', url)
        r = requests.get(url)
        self.assertEqual(r.status_code, 200)

    def test_002_fedora_content_of_response(self):
        url = self.baseurl+'fedora/describe'
        print('URL:', url)
        r = requests.get(url)
        to_be_found = "Repository Information" 
        self.assertEqual( r.text.find(to_be_found)>0 , True)

    def test_003_fedora_login(self):
        url = self.baseurl+'fedora/objects/fedora-system:ServiceDefinition-3.0/objectXML'
        print('URL:', url)
        r = requests.get(url, auth=(self.username, self.password))
        self.assertEqual(r.status_code, 200)

    def test_004_fedora_new_object(self):
        url = self.baseurl+'fedora/objects/new'
        print('URL:', url)
        r = requests.post(url, auth=(self.username, self.password))
        self.assertEqual(r.status_code, 201)

    def test_005_fedora_non_existant_object(self):
        url = self.baseurl+'fedora/objects/doesnotexist'
        print('URL:', url)
        r = requests.post(url, auth=(self.username, self.password))
        self.assertEqual(r.status_code, 404, "should have been 404, was " + str(r.status_code) + "\n" + r.text)

    def test_006_gsearch_service_up(self):
        url = self.baseurl+'fedoragsearch'
        print('URL:', url)
        r = requests.get(url, auth=(self.username, self.password))
        self.assertEqual(r.status_code, 200)

    def test_007_gsearch_content_of_response(self):
        url = self.baseurl+'fedoragsearch/rest'
        print('URL:', url)
        r = requests.get(url, auth=(self.username, self.password))
        to_be_found = "Admin Client for Fedora Generic Search Service" 
        self.assertEqual( r.text.find(to_be_found)>0 , True)


unittest.main(argv=[''], verbosity=3, exit=True)
