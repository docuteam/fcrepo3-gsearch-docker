FROM tomcat:8.5-jre8-alpine
MAINTAINER Jonathan Green <jonathan@razzard.com>

ENV JAVA_OPTS="-Xms2g -Xmx2g -XX:NewSize=256m -XX:MaxNewSize=356m" \
    CATALINA_TMPDIR=/tmp \
    FEDORA_HOME=/opt/fedora \
    FEDORA_ADMIN_PASSWORD=fedoraAdmin \
    FEDORA_USER_NAME=fedoraUser \
    FEDORA_USER_PASSWORD=fedoraUser \
    FEDORA_SERVER_HOST=localhost \
    FEDORA_DB_TYPE=derby \
    FEDORA_DEFAULT_NAMESPACE=default \
    FEDORA_DB_USER=fedora \
    FEDORA_DB_PASSWORD=fedora \
    FEDORA_DB_HOST=localhost \
    FEDORA_DB_NAME=fedora3

COPY . /build

RUN /build/scripts/tomcat.sh
RUN /build/scripts/confd.sh
RUN /build/scripts/fedora.sh
RUN /build/scripts/gsearch.sh

# Expose port 8080 and start tomcat
WORKDIR /build/scripts
EXPOSE 8080
CMD ["./start.sh"]
